#!/bin/bash -e

function install_marelle() {
  sudo apt-get update
  sudo apt-get install -y curl git swi-prolog-nox
  git clone https://github.com/larsyencken/marelle /usr/local/marelle
  cat >/usr/local/bin/marelle <<EOF
#!/bin/bash
exec swipl -q -t main -s /usr/local/marelle/marelle.pl "\$@"
EOF
  chmod a+x /usr/local/bin/marelle
}

function checkout_deps() {
  mkdir -p ~/.local
  if [ ! -d ~/.local/juici-server-standalone ]; then
    git co https://bitbucket.org/larsyencken/juici-server-standalone
  fi
  mkdir -p ~/.marelle
  if [ ! -d ~/.marelle/deps ]; then
    ln -s ~/.local/juici-server-standalone/marelle-deps ~/.marelle/deps
  fi
}

which -s marelle || install_marelle

test -d ~/.marelle/deps || checkout_deps

marelle meet juici-server-standalone
