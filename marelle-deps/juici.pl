%
%  juici.pl
%  marelle-deps
%

meta_pkg('juici-server-standalone', [
    juici,
    mongodb
]).

meta_pkg(juici, [
    ruby,
    gem,
    'rvm/1.9.3-p194',
    '__juici source checked out'
]).

command_pkg(ruby).

command_pkg(gem).
installs_with_apt(gem, rubygems).

git_step(
    '__juici source checked out',
    'https://github.com/richo/juici',
    '~/.local/juici'
).

command_pkg(rvm).

pkg('rvm/1.9.3-p194').

rvm_version(P, V) :- atom_concat('rvm/', V, P).

met(P, _) :-
    rvm_version(P, V), !,
    bash(['rvm list 2>/dev/null | fgrep -q ', V]).
meet(P, _) :-
    rvm_version(P, V), !,
    bash(['rvm install ', V]).
depends(P, _, [rvm]) :- rvm_version(P, _).

command_pkg(rvm).
meet(rvm, linux(_)) :-
    bash('curl -L https://get.rvm.io | bash -s stable --ruby --autolibs=enable --auto-dotfiles').
depends(rvm, _, [ruby]).
